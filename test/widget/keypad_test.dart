// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:calculator/calculator_bloc.dart';
import 'package:calculator/keypad.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockCalculatorBloc extends Mock implements CalculatorBloc {}

void main() {
  group("Test keypad interaction", () {
    testWidgets('Keypad test', (WidgetTester tester) async {
      var mockBloc = MockCalculatorBloc();
      // Build our app and trigger a frame.
      // Use materialApp because of SafeArea widget, sizedbox/center because widget test runs in fixed size of H:W 600:800px and column because we need the flex for expanded
      var keyPad = MaterialApp(
        home: SizedBox(
          height: 600,
          width: 800,
          child: Center(
            child: Column(
              children: <Widget>[
                KeyPad(mockBloc),
              ],
            ),
          ),
        ),
      );

      await tester.pumpWidget(keyPad);

      // Verify that our counter starts at 0.
      var button7 = find.text('7');
      expect(button7, findsOneWidget);
      expect(find.text('='), findsOneWidget);
      expect(find.text('DEL'), findsOneWidget);
      expect(find.text('-'), findsOneWidget);

      await tester.tap(button7);
      await tester.pump();
      verify(mockBloc.inputNumber("7"));
    });
  });
}
