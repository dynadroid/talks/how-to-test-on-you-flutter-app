import 'package:calculator/calculator_bloc.dart';
import 'package:test/test.dart';

void main() {
  expressionGenerationTests();
  calculationTests();
}

void calculationTests() {
  group("Calculation tests", () {
    CalculatorBloc bloc;
    setUp(() {
      bloc = CalculatorBloc();
    });

    test("Addition 1+1=2.0", () {
      bloc.inputNumber("1");
      bloc.inputOperator("+");
      bloc.inputNumber("1");
      expect(bloc.computedValue, "2.0");
    });

    test("Subtraction 1-1=0.0", () {
      bloc.inputNumber("1");
      bloc.inputOperator("-");
      bloc.inputNumber("1");
      expect(bloc.computedValue, "0.0");
    });

    test("Multiplication 2\u00D72=4.0", () {
      bloc.inputNumber("2");
      bloc.inputOperator("\u00D7");
      bloc.inputNumber("2");
      expect(bloc.computedValue, "4.0");
    });

    test("Division 4\u00F72=2.0", () {
      bloc.inputNumber("4");
      bloc.inputOperator("\u00F7");
      bloc.inputNumber("2");
      expect(bloc.computedValue, "2.0");
    });
  });
}

void expressionGenerationTests() {
  group("Test expression generation", () {
    CalculatorBloc bloc;
    setUp(() {
      bloc = CalculatorBloc();
    });

    test("number before operator", () {
      bloc.inputNumber("1");
      expect(bloc.exprLeft, "1");
    });

    test("operator", () {
      bloc.inputNumber("1");
      bloc.inputOperator("+");

      expect(bloc.operatorToken.label, "+");
    });

    test("number after operator", () {
      bloc.inputNumber("1");
      bloc.inputOperator("+");
      bloc.inputNumber("1");

      expect(bloc.exprRight, "1");
    });

    test("only allow 1 operator", () {
      bloc.inputNumber("1");
      bloc.inputOperator("+");
      bloc.inputNumber("1");
      bloc.inputOperator("-");

      expect(bloc.operatorToken.label, "+");
      expect(bloc.exprRight, allOf(contains("1"), isNot(contains("-"))));
    });
  });
}
