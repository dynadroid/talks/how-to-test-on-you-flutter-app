import 'package:flutter/material.dart';

import 'calculator_bloc.dart';
import 'keypad.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue, textTheme: TextTheme()),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final calculatorBloc = CalculatorBloc();

  @override
  Widget build(BuildContext context) {
    final TextStyle calculationStyle = Theme.of(context).textTheme.display3;
    final TextStyle outcomeStyle = Theme.of(context).textTheme.display2;

    return Scaffold(
      appBar: AppBar(
        elevation: 4,
      ),
      body: Column(
        children: <Widget>[
          // Top view with the calculation and result
          Expanded(
            flex: 3,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      StreamBuilder<String>(
                          stream: calculatorBloc.input,
                          builder: (context, snapshot) {
                            if (snapshot.hasError || snapshot.data == null) {
                              return Container();
                            }
                            return Text(
                              snapshot.data,
                              key: Key("input"),
                              style: calculationStyle,
                            );
                          }),
                      StreamBuilder<String>(
                          stream: calculatorBloc.output,
                          builder: (context, snapshot) {
                            if (snapshot.hasError || snapshot.data == null) {
                              return Container();
                            }
                            return Text(snapshot.data,
                                key: Key("output"), style: outcomeStyle);
                          })
                    ],
                  ),
                ),
              ],
            ),
          ),
          new KeyPad(calculatorBloc),
        ],
      ),
    );
  }
}
