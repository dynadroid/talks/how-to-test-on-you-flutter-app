import 'package:calculator/calculator_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class KeyPad extends StatelessWidget {
  const KeyPad(
    this._bloc, {
    Key key,
  }) : super(key: key);

  final CalculatorBloc _bloc;
  @override
  Widget build(BuildContext context) {
    final TextStyle numberStyle = Theme.of(context).textTheme.display3;
    final TextStyle operatorStyle = Theme.of(context).textTheme.display1;

    return Expanded(
      flex: 5,
      child: Container(
        color: Colors.blue,
        child: SafeArea(
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    NumberKey(number: "7", style: numberStyle, bloc: _bloc),
                    NumberKey(number: "4", style: numberStyle, bloc: _bloc),
                    NumberKey(number: "1", style: numberStyle, bloc: _bloc),
                    NumberKey(number: ",", style: numberStyle, bloc: _bloc),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    NumberKey(number: "8", style: numberStyle, bloc: _bloc),
                    NumberKey(number: "5", style: numberStyle, bloc: _bloc),
                    NumberKey(number: "2", style: numberStyle, bloc: _bloc),
                    NumberKey(number: "0", style: numberStyle, bloc: _bloc),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    NumberKey(number: "9", style: numberStyle, bloc: _bloc),
                    NumberKey(number: "6", style: numberStyle, bloc: _bloc),
                    NumberKey(number: "3", style: numberStyle, bloc: _bloc),
                    FunctionKey(
                        label: "=",
                        style: numberStyle,
                        function: () {
                          _bloc.computeExpression(clear: true);
                        }),
                  ],
                ),
              ),
              Container(
                width: 2,
                color: Colors.grey,
              ),
              Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  FunctionKey(
                    label: "DEL",
                    style: operatorStyle,
                    function: () {
                      _bloc.removeInput();
                    },
                  ),
                  OperatorKey(
                    operator: "\u00F7",
                    style: operatorStyle,
                    bloc: _bloc,
                  ),
                  OperatorKey(
                    operator: "\u00D7",
                    style: operatorStyle,
                    bloc: _bloc,
                  ),
                  OperatorKey(
                    operator: "+",
                    style: operatorStyle,
                    bloc: _bloc,
                  ),
                  OperatorKey(
                    operator: "-",
                    style: operatorStyle,
                    bloc: _bloc,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FunctionKey extends StatelessWidget {
  const FunctionKey({
    Key key,
    @required label,
    @required style,
    @required VoidCallback function,
  })  : _label = label,
        _style = style,
        _function = function,
        super(key: key);

  final TextStyle _style;
  final VoidCallback _function;
  final String _label;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FlatButton(
        key: Key(_label),
        child: Text(
          _label,
          style: _style,
        ),
        onPressed: _function,
      ),
    );
  }
}

class NumberKey extends StatelessWidget {
  const NumberKey({
    Key key,
    @required number,
    @required style,
    @required CalculatorBloc bloc,
  })  : _number = number,
        _style = style,
        _bloc = bloc,
        super(key: key);

  final TextStyle _style;
  final CalculatorBloc _bloc;
  final String _number;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FlatButton(
        key: Key(_number),
        child: Text(
          _number,
          style: _style,
        ),
        onPressed: () {
          _bloc.inputNumber(_number);
        },
      ),
    );
  }
}

class OperatorKey extends StatelessWidget {
  const OperatorKey({
    Key key,
    @required operator,
    @required style,
    @required CalculatorBloc bloc,
  })  : _operator = operator,
        _style = style,
        _bloc = bloc,
        super(key: key);

  final TextStyle _style;
  final CalculatorBloc _bloc;
  final String _operator;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FlatButton(
        key: Key(_operator),
        child: Text(
          _operator,
          style: _style,
        ),
        onPressed: () {
          _bloc.inputOperator(_operator);
        },
      ),
    );
  }
}
