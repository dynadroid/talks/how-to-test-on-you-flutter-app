import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:math_expressions/math_expressions.dart';

class CalculatorBloc {
  final StreamController<String> _inputController = StreamController<String>();
  final StreamController<String> _outputController = StreamController<String>();

  // should be passed through the constructor as dependency in an ideal world so you
  // can mock it out.
  final Parser _parser = Parser();
  final ContextModel _cm = new ContextModel();

  final Map<String, String> _operatorTokenMap = {
    "\u00F7": "/",
    "\u00D7": "*",
    "+": "+",
    "-": "-",
  };

  Stream<String> get input => _inputController.stream;
  Stream<String> get output => _outputController.stream;
  String computedValue = "";
  String exprLeft = "";
  String exprRight = "";
  OperatorToken operatorToken;

  void inputNumber(String value) {
    if (operatorToken == null) {
      if (value == ",") {
        if (exprLeft.isNotEmpty && !exprLeft.contains(".")) {
          exprLeft += value;
        }
      } else {
        exprLeft += value;
      }
    } else {
      if (value == ",") {
        if (exprRight.isNotEmpty && !exprRight.contains(".")) {
          exprRight += value;
        }
      } else {
        exprRight += value;
      }
    }
    _formatAndPushExpression();
    computeExpression();
  }

  void _formatAndPushExpression() {
    var operator;
    if (operatorToken != null) {
      operator = operatorToken.label;
    } else {
      operator = "";
    }
    _inputController.add(exprLeft + operator + exprRight);
  }

  void inputOperator(String operator) {
    if (exprLeft.isEmpty) {
      return;
    }

    if (exprRight.isEmpty) {
      operatorToken =
          OperatorToken(token: _operatorTokenMap[operator], label: operator);
      _formatAndPushExpression();
      computeExpression();
    }
  }

  void removeInput() {
    if (operatorToken == null) {
      if (exprLeft.isNotEmpty) {
        exprLeft = exprLeft.substring(0, exprLeft.length - 1);
      }
    } else {
      if (exprRight.isEmpty) {
        operatorToken = null;
      } else {
        exprRight = exprRight.substring(0, exprRight.length - 1);
      }
    }
    _formatAndPushExpression();
    computeExpression();
  }

  void onDispose() {
    _inputController.close();
    _outputController.close();
  }

  void computeExpression({clear = false}) {
    if (exprLeft.isNotEmpty && exprRight.isNotEmpty && operatorToken != null) {
      String expression = exprLeft + operatorToken.token + exprRight;
      try {
        computedValue = _parser
            .parse(expression)
            .evaluate(EvaluationType.REAL, _cm)
            .toString();
        if (clear) {
          exprLeft = computedValue;
          operatorToken = null;
          exprRight = "";
          _outputController.sink.add("");
          _inputController.sink.add(computedValue);
          computedValue = "";
        } else {
          _outputController.sink.add(computedValue);
        }
      } on StateError catch (e) {
        // TODO show error
        _outputController.sink.add(e.message);
      }
    } else {
      _outputController.sink.add("");
    }
  }
}

class OperatorToken {
  final String token;
  final String label;

  OperatorToken({@required this.token, @required this.label});
}
