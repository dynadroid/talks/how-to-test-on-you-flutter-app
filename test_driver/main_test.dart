import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group("Calculator App", () {
    final inputTextFinder = find.byValueKey("input");
    final outputTextFinder = find.byValueKey("output");
    final button1 = find.byValueKey("1");
    final button9 = find.byValueKey("9");
    final buttonPlus = find.byValueKey("+");

    FlutterDriver driver;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    // button1 is actually a button and getText looks for a Text widget with a text.
    test('find number 1', () async {
      expect(await driver.getText(button1), "1");
    }, skip: true);
    test('find number 9', () async {
      expect(await driver.getText(button9), "9");
    }, skip: true);

    test('1+9=10.0', () async {
      await driver.tap(button1);
      await driver.tap(buttonPlus);
      await driver.tap(button9);

      expect(await driver.getText(inputTextFinder), "1+9");
      expect(await driver.getText(outputTextFinder), "10.0");
    });
  });
}
