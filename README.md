# Calculator Sample

A sample flutter app created for giving a talk about testing on flutter slides can be found here [slides](https://docs.google.com/presentation/d/1VDObgMLSma70lLnfs3rGjpoQNKn2jPTZtjetfkhbaTA/edit?usp=sharing)

[![Codemagic build status](https://api.codemagic.io/apps/5db38d0a7d3edb4b244691f4/5db38d0a7d3edb4b244691f3/status_badge.svg)](https://codemagic.io/apps/5db38d0a7d3edb4b244691f4/5db38d0a7d3edb4b244691f3/latest_build)

## Getting Started

Clone the project and run

```
flutter test --coverage
genhtml coverage/lcov.info -o coverage
```

Generates the coverage report for the test located in [coverage/html/index.html](coverage/html/index.html)

```
flutter run

```

To build and and install the app on a connect device.q
